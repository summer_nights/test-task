<?php

$host = "localhost";
$user = "test";
$pass = "test1234";
$db = "test";


$dsn = "mysql:host=$host;dbname=$db";


$pdo = new PDO($dsn, $user, $pass);





function getCat($connect){

    $res = $connect->query('SELECT * FROM folders');

    //Создаем масив где ключ массива является ID
    $cat = array();
    while($row = $res->fetch()){
        $cat[$row['id']] = $row;
    }
    return $cat;
}


function getTree($dataset) {
    $tree = array();

    foreach ($dataset as $id => &$node) {
        //Если нет вложений
        if (!$node['parent']){
            $tree[$id] = &$node;
        }else{
            //Если есть потомки то перебераем массив
            $dataset[$node['parent']]['childs'][$id] = &$node;
        }
    }
    return $tree;
}

//Получаем подготовленный массив с данными
$cat  = getCat($pdo);

//Создаем древовидное меню
$tree = getTree($cat);

//Шаблон для вывода меню в виде дерева
function tplMenu($category){
    $menu = '<li>
        <a href="#" id="link" title="'. $category['title'] .'">'.
        $category['title'].'</a>';

    if(isset($category['childs'])){
        $menu .= '<ul class="grey">'. showCat($category['childs']) .'</ul>';
    }
    $menu .= '</li>';

    return $menu;
}

/**
 * Рекурсивно считываем наш шаблон
 **/
function showCat($data){
    $string = '';
    foreach($data as $item){
        $string .= tplMenu($item);
    }
    return $string;
}

//Получаем HTML разметку
$cat_menu = showCat($tree);

//Выводим на экран
echo '<ul>'. $cat_menu .'</ul>';

?>

<link rel="stylesheet" href="style.css">
<script src="script.js"></script>