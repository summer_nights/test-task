В файле index.php задаются параметры для подлючение БД.

В базе необходимо создать таблицу:

CREATE TABLE IF NOT EXISTS `folders` (
`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
`title` varchar(255) NOT NULL,
`parent` int(10) unsigned NOT NULL,
PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

Пример данных для проверки:
INSERT INTO `categories` (`title`, `parent`) VALUES
('Автомобили', 0),
('Мотоциклы', 0),
('Мазда', 1),
('Хонда', 1),
('Кавасаки', 2),
('Харлей', 2),
('Мазда 3', 3),
('Мазда 6', 3),
('Седан', 7),
('Хечбэк', 7),
('Лодки', 0),
('Лифтбэк', 8),
('Кроссовер', 8),
('Белый', 13),
('Красный', 13),
('Черный', 13),
('Зеленый', 13),
('Мазда CX', 3),
('Мазда MX', 3);